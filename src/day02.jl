using BenchmarkTools
using Revise
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day02.txt")) do f
        while !eof(f)
            line = readline(f)
            line = split(line, ":")[2]
            shows = split(line, ";")
            draws = []
            for show in shows
                show = show[2:end]
                info = split(show, " ")
                draw = Dict()
                for i in 1:(length(info) / 2)
                    value = parse(Int, info[Int((i - 1) * 2 + 1)])
                    color = info[Int(i * 2)]
                    if color[end] == ','
                        color = color[begin:(end - 1)]
                    end
                    draw[color] = value
                end
                push!(draws, draw)
            end
            push!(data, draws)
        end
    end
    return data
end

function solve_problem(data)
    res = 0
    for draws in data
        maxes = Dict("red" => 0, "green" => 0, "blue" => 0)
        for draw in draws
            for color in keys(draw)
                if draw[color] > maxes[color]
                    maxes[color] = draw[color]
                end
            end
        end
        res += prod(values(maxes))
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)
    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
