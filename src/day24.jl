using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day24.txt")) do f
        while !eof(f)
            line = readline(f)
            pos, vel = split(line, " @ ")
            pos = split(pos, ", ")
            vel = split(vel, ", ")
            pos = parse.(Int, pos)
            vel = parse.(Int, vel)
            push!(data, (pos, vel))
        end
    end
    return data
end

function calc_component_intersect(s1, s2)
    # "getting the linear component line for:" |> display
    # "$s1 and $s2" |> display
    p1, v1 = s1
    p2, v2 = s2
    # "this is:" |> display
    # (v2 / v1, (p2 - p1) / v1) |> display
    return v2 / v1, (p2 - p1) / v1
end

# Returns value, everywhere, nowhere
function intersect_linears(a1, b1, a2, b2)
    if a1 ≈ a2
        if b1 == b2
            return 0, true, false
        else
            return 0, false, true
        end
    end
    # "linear intersect is at $((b2 - b1) / (a1 - a2))" |> display
    return (b2 - b1) / (a1 - a2), false, false
end

# Return intersect (l, t), everywhere, nowhere, future
function calc_intersect(stone1, stone2)
    a1, b1 = calc_component_intersect(getindex.(stone1, 1), getindex.(stone2, 1))
    a2, b2 = calc_component_intersect(getindex.(stone1, 2), getindex.(stone2, 2))
    l, everywhere, nowhere = intersect_linears(a1, b1, a2, b2)
    if nowhere || everywhere
        return (0, 0), everywhere, nowhere, false
    end
    t = a1 * l + b1
    # t |> display

    p1, v1 = getindex.(stone1, 1)
    p2, v2 = getindex.(stone1, 2)
    x = p1 + v1 * t
    y = p2 + v2 * t
    # println("stones $stone1, $stone2 intersecting")
    # println("at coords $x, $y")
    # println("at params $t, $l")
    if t <= 0 || l <= 0
        # println("intersect in past for stones $stone1, $stone2")
        return (0, 0), false, false, true
    end
    return (x, y), false, false, false
end

function is_valid(x, y, range)
    s, e = range
    return s <= x <= e && s <= y <= e
end

function solve_problem(data)
    range = 200000000000000, 400000000000000
    res = 0
    for i in eachindex(data)
        for j in i+1:length(data)
            stone1 = data[i]
            stone2 = data[j]
            intersect, everywhere, nowhere, future  = calc_intersect(stone1, stone2)
            if nowhere
                # do nothing
            elseif everywhere
                @error("assume everywhere does not happen for now")
            elseif future 
                # one intersect is in past, do nothing
            elseif is_valid(intersect..., range)
                res += 1
            end
        end
    end
    return res
end

function solve_part_two(data)
    # p1, v1 = data[1]
    # p2, v2 = data[2]
    # p3, v3 = data[3]
    # # line starts at p1 
    # # Test if line can intersect stone j at lambda = 1
    # for j = eachindex(data)[2:end]
    #     pj, vj = data[j]
    #     # Then we find the intersection with j + 1
    #     if j != length(data)
    #         p_other, v_other = data[j+1]
    #     else
    #         p_other, v_other = data[2]
    #     end
    #     # The other lambda is then:
    #     l3_1 = (p_other[1] - p1[1]) / (pj[1] - p1[1] + vj[1] - v_other[1])
    #     l3_2 = (p_other[2] - p1[2]) / (pj[2] - p1[2] + vj[2] - v_other[2])
    #     if l3_1 ≈ l3_2
    #         println(l3_1)
    #     end
    # end

    for (pos, vel) in data
        if sum(vel) == 0
            return sum(pos)
        end
    end
end

function main()
    data = parse_data()
    sol = solve_part_two(data)
    println(sol)
end

main()
