using BenchmarkTools

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day14.txt")) do f
        while !eof(f)
            line = readline(f)
            push!(data, collect(line))
        end
    end
    return data
end

function tilt_north(data)
    for i in eachindex(data)[2:end]
        for j in eachindex(data[i])
            if data[i][j] == 'O'
                # See how far up it can move
                stopped = false
                look_back = 1
                while true
                    stopped = data[i-look_back][j] != '.'
                    if stopped
                        break
                    else
                        look_back += 1
                        if i - look_back == 0
                            break
                        end
                    end
                end
                if look_back != 1
                    data[i][j] = '.'
                    data[i-look_back + 1][j] = 'O'
                end
            end
        end
    end
    return data
end

function tilt_south(data)
    # Iterate other way around
    for i in reverse(eachindex(data))[2:end]
        for j in eachindex(data[i])
            if data[i][j] == 'O'
                # See how far up it can move
                stopped = false
                look_back = 1
                while true
                    stopped = data[i+look_back][j] != '.'
                    if stopped
                        break
                    else
                        look_back += 1
                        if i + look_back == length(data) + 1
                            break
                        end
                    end
                end
                if look_back != 1
                    data[i][j] = '.'
                    data[i+look_back - 1][j] = 'O'
                end
            end
        end
    end
    return data
end

function tilt_west(data)
    for i in eachindex(data[1])[2:end]
        for j in eachindex(data)
            if data[j][i] == 'O'
                # See how far up it can move
                stopped = false
                look_back = 1
                while true
                    stopped = data[j][i-look_back] != '.'
                    if stopped
                        break
                    else
                        look_back += 1
                        if i - look_back == 0
                            break
                        end
                    end
                end
                if look_back != 1
                    data[j][i] = '.'
                    data[j][i-look_back + 1] = 'O'
                end
            end
        end
    end
    return data
end


function tilt_east(data)
    for i in reverse(eachindex(data[1]))[2:end]
        for j in eachindex(data)
            if data[j][i] == 'O'
                # See how far up it can move
                stopped = false
                look_back = 1
                while true
                    stopped = data[j][i+look_back] != '.'
                    if stopped
                        break
                    else
                        look_back += 1
                        if i + look_back == length(data[1]) + 1
                            break
                        end
                    end
                end
                if look_back != 1
                    data[j][i] = '.'
                    data[j][i + look_back - 1] = 'O'
                end
            end
        end
    end
    return data
end

function show(data)
    for line in data
        println(join(line, ""))
    end
    println()
end

function compute_sol(data)
    sol = 0
    for (i, line) in enumerate(data)
        for char in line
            if char == 'O'
                sol += length(data) - i + 1
            end
        end
    end
    return sol    
end

function solve_problem(data)
    known_states = Dict()
    i = 0
    stop = 1_000_000_000
    once = true
    while i != stop
        if (data in keys(known_states)) && once
            # We have encountered this sate at 
            prev = known_states[data]
            cur = i
            jump = cur - prev
            to_go = stop - cur
            full_steps = div(to_go, jump)
            i += full_steps * jump
            once = false
        end
        known_states[data] = i
        data = tilt_north(data)
        data = tilt_west(data)
        data = tilt_south(data)
        data = tilt_east(data)
        i += 1
    end
    return compute_sol(data)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
end

main()
