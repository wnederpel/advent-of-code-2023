using BenchmarkTools
using Revise
using Profile
using PProf

function parse_data()
    nodes = Dict()
    instructions = ""
    open(joinpath(dirname(@__FILE__), "../data/day08.txt")) do f
        i = 1
        while !eof(f)
            line = readline(f)
            if i == 1
                instructions = line
            elseif i == 2
                # Do nothing
            else
                start, lr = split(line, " = ")
                lr = lr[(begin + 1):(end - 1)]
                lr = split(lr, ", ")
                nodes[start] = lr
            end
            i += 1
        end
    end
    return instructions, nodes
end

@inbounds function solve_problem_lcm(data)
    instructions, nodes = data
    cur_nodes = []
    for node in keys(nodes)
        if node[3] == 'A'
            push!(cur_nodes, node)
        end
    end

    # Ok so the loop is too slow
    # We can detect when start node 1 hits an end point etc. 
    # Then we need to know how long till it would come there again
    # The second time we know how long is in between
    # first_hits = Int.(zeros(length(cur_nodes)))
    first_hits = Vector{Int}(undef, length(cur_nodes))
    fill!(first_hits, 0)
    len_instructions = length(instructions)
    for j in eachindex(cur_nodes)
        cur_node = cur_nodes[j]
        loops = 0
        i = 1
        while true
            next_nodes = nodes[cur_node]
            if instructions[i] == 'L'
                cur_node = next_nodes[1]
            else
                cur_node = next_nodes[2]
            end

            if cur_node[3] == 'Z'
                first_hits[j] = i + loops * len_instructions
                break
            end
            i += 1
            if i > len_instructions
                loops += 1
                i = 1
            end
        end
    end
    return lcm(first_hits...)
end

@inbounds function solve_problem(data)
    instructions, nodes = data
    cur_nodes = []
    for node in keys(nodes)
        if node[3] == 'A'
            push!(cur_nodes, node)
        end
    end
    tot = 1
    i = 1
    # Ok so the loop is too slow
    # We can detect when start node 1 hits an end point etc. 
    # Then we need to know how long till it would come there again
    # The second time we know how long is in between
    # first_hits = Int.(zeros(length(cur_nodes)))
    first_hits = Vector{Tuple{Int,Int}}(undef, length(cur_nodes))
    fill!(first_hits, (0, 0))
    second_hits = Vector{Int}(undef, length(cur_nodes))
    fill!(second_hits, 0)
    while true
        instr = instructions[i]
        new_cur_nodes = Vector(undef, length(cur_nodes))
        for j in eachindex(cur_nodes)
            if instr == 'L'
                new_cur_nodes[j] = nodes[cur_nodes[j]][1]
            else
                new_cur_nodes[j] = nodes[cur_nodes[j]][2]
            end
        end
        cur_nodes = new_cur_nodes
        for (k, cur_node) in enumerate(cur_nodes)
            if endswith(cur_node, "Z")
                if first_hits[k][1] == 0
                    first_hits[k] = (tot, i)
                elseif second_hits[k] == 0 && first_hits[k][2] == i
                    second_hits[k] = tot
                end
            end
        end

        if length(filter(x -> x == 0, second_hits)) == 0
            return get_results(first_hits, second_hits, length(cur_nodes))
        end

        i += 1
        tot += 1
        if i > length(instructions)
            i = 1
        end
    end
end

@inbounds function get_results(first_hits, second_hits, len_nodes)
    dist = Int.(zeros(len_nodes))
    for k in 1:len_nodes
        dist[k] = second_hits[k] - first_hits[k][1]
    end
    # Now we know that node K will finish at first[K] and every first[K] + dist[K] * N_k afterwards
    # So we now seek to find a value b such that b = first[K] + dist[K] * N_k
    # Try all possible b's for k = 1 and check if others _can_ match
    max_k = 0
    max_dist = 0
    for k in 1:len_nodes
        if dist[k] > max_dist
            max_k = k
            max_dist = dist[k]
        end
    end
    first_max = first_hits[max_k][1]
    n = 0
    while true
        b = first_max + max_dist * n
        done = true
        for k in 1:len_nodes
            n_k = (b - first_hits[k][1]) / dist[k]
            # n_k should be an int, then its all good.
            # If not we're not done
            if abs(round(n_k) - n_k) > 0.00001
                done = false
                break
            end
        end
        if done
            return b
        end
        n += 1
    end
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)

    # Profile.clear()
    # @profile (
    #     for i = 1:500
    #         solve_problem_lcm(data)
    #     end
    # )
    # pprof()

    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)

    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
