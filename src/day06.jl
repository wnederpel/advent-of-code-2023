using BenchmarkTools
using Profile
using PProf

function parse_data()
    times = []
    distances = []
    open(joinpath(dirname(@__FILE__), "../data/day06.txt")) do f
        i = 1
        while !eof(f)
            line = readline(f)
            if i == 1
                line = split(line, ":      ")[2]
                times = parse(Int, line)
            end
            if i == 2
                line = split(line, ":  ")[2]
                distances = parse(Int, line)
            end
            if i >= 3
                break
            end

            i += 1
        end
    end
    return times, distances
end

function solve_problem(data)
    time, distance = data
    time2 = time / 2
    D = sqrt(time2^2 - distance)
    x1 = (time2 + D)
    x2 = (time2 - D)
    c = ceil(x2)
    if c == x2
        s = c + 1
    else
        s = c
    end
    f = floor(x1)
    if f == x1
        e = f - 1
    else
        e = f
    end
    return (e - s + 1)
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)
    # data = parse_data()
    # Profile.clear()
    # @profile (
    #     for i = 1:1000000
    #         solve_problem(data)
    #     end
    # )
    # pprof()
    # @benchmark data = parse_data() seconds=0.1
    # @benchmark data = parse_data() seconds=1
    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)
    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
    # @benchmark solve_problem($data)
end

main()
