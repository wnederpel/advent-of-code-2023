using BenchmarkTools
using DataStructures
using Profile
using PProf
using ShiftedArrays

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day07.txt")) do f
        while !eof(f)
            line = readline(f)
            hand, bid = split(line, " ")
            bid = parse(Int, bid)
            push!(data, [hand, bid])
        end
    end
    return data
end

@inbounds function turn_to_bins(hand)
    # d = DefaultDict(0)
    # d = Dict(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0)
    d = [0, 0, 0, 0, 0, 0]
    # for card in unique(hand)
    count_dict = counter(hand)
    for (card, count) in count_dict
        if card != 'J'
            d[count += 1] += 1
        end
    end
    # All available joker will always add to the highest match there already is
    max_match = 1
    for key in 6:-1:1
        if d[key] > 0
            max_match = key
            break
        end
    end
    # Ik heb nu max_val matches van max_match
    # Dat worden er max_val - 1 matches, en ik krijg er een match bij op max_match + J's
    d[max_match] -= 1
    d[max_match + length(filter(x -> x == 'J', hand))] += 1
    return d
end

@inbounds function break_tie(hand1, hand2)
    for (char1, char2) in zip(hand1, hand2)
        o1, o2 = order[char1], order[char2]
        if o1 < o2
            return true
        elseif o1 > o2
            return false
        end
    end
end

@inbounds function poker_order(entry1, entry2)
    hand1, _, bins1 = entry1
    hand2, _, bins2 = entry2

    b12, b13, b14, b15 = bins1[3], bins1[4], bins1[5], bins1[6]
    b22, b23, b24, b25 = bins2[3], bins2[4], bins2[5], bins2[6]

    if 1 == b15 && 1 == b25
        return break_tie(hand1, hand2)
    elseif b15 == 1
        return false
    elseif b25 == 1
        return true
    end

    if b14 == 1 && b24 == 1
        return break_tie(hand1, hand2)
    elseif b14 == 1
        return false
    elseif b24 == 1
        return true
    end

    if (b13 == 1 && b12 == 1) && (b23 == 1 && b22 == 1)
        return break_tie(hand1, hand2)
    elseif (b13 == 1 && b12 == 1)
        return false
    elseif (b23 == 1 && b22 == 1)
        return true
    end

    if b13 == 1 && b23 == 1
        return break_tie(hand1, hand2)
    elseif b13 == 1
        return false
    elseif b23 == 1
        return true
    end

    if b12 == 2 && b22 == 2
        return break_tie(hand1, hand2)
    elseif b12 == 2
        return false
    elseif b22 == 2
        return true
    end

    if b12 == 1 && b22 == 1
        return break_tie(hand1, hand2)
    elseif b12 == 1
        return false
    elseif b22 == 1
        return true
    end

    return break_tie(hand1, hand2)
end

function solve_problem(data)
    global order = Dict(
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'T' => 10,
        '9' => 9,
        '8' => 8,
        '7' => 7,
        '6' => 6,
        '5' => 5,
        '4' => 4,
        '3' => 3,
        '2' => 2,
        'J' => 0,
    )
    for datum in data
        hand, _ = datum
        bins = turn_to_bins(hand)
        push!(datum, bins)
    end

    sort!(data; lt=poker_order)
    res = 0
    for i in 1:length(data)
        res += i * data[i][2]
    end
    return res
end

function main()
    data = parse_data()
    sol = solve_problem(data)
    println(sol)

    # Profile.clear()
    # @profile (
    #     for i = 1:1000
    #         solve_problem(data)
    #     end
    # )
    # pprof()
    # b = @benchmarkable parse_data()
    # tune!(b)
    # run(b)

    b = @benchmarkable solve_problem($data)
    tune!(b)
    run(b)
end

main()
