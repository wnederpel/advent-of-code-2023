using BenchmarkTools
using DataStructures
using Revise
using Profile
using PProf

function parse_data()
    data = []
    open(joinpath(dirname(@__FILE__), "../data/day12.txt")) do f
        while !eof(f)
            line = readline(f)
            splitted = split(line, " ")
            counts = parse.(Int, split(splitted[2], ","))
            push!(data, (splitted[1], counts))
        end
    end
    return data
end

function count_occurences(line, counts)
    # Remove all starting and trailing dots
    line = lstrip(line, '.')
    

    if line == "" 
        return Int(counts == [])
    end
    if isempty(counts)
        return Int(!('#' in line))
    end

    if (line, counts) in keys(cache)
        return cache[(line, counts)]
    end


    # Now line starts with either # or ?
    if line[1] == '#'
        # If it starts with # try to match a thing in counts
        if length(line) < counts[1] || '.' in line[begin:counts[1]]
            cache[(line, counts)] = 0
            return 0 
        elseif length(line) == counts[1]
            cache[(line, counts)] = Int(length(counts) == 1)
            return cache[(line, counts)]
        elseif line[counts[1] + 1] == '#'
            cache[(line, counts)] = 0
            return 0
        else
            cache[(line, counts)] = count_occurences(line[counts[1] + 2:end], copy(counts[2:end]))
            return cache[(line, counts)]
        end
    end
    cache[(line, counts)] = count_occurences('#' * line[2:end], counts) + count_occurences(line[2:end], counts)
    # So it starts with a ?
    return cache[(line, counts)]
end


function solve_problem(data, repeats)
    global cache = Dict()
    res = 0
    for (index, entry) in enumerate(data)
        begin_line, begin_counts = entry

        line = repeat(begin_line * '?', repeats)[begin:end-1]
        counts = repeat(begin_counts, repeats)

        options = count_occurences(
            line, counts
        )

        res += options
    end
    return res
end

function main()
    data = parse_data()
    @time sol = solve_problem(data, 1)
    println(sol)
    @time sol = solve_problem(data, 5)
    println(sol)

    Profile.clear()
    @profile (
        for i in 1:10
            solve_problem(data, 5)
        end
    )
    pprof()

    b = @benchmarkable solve_problem($data, 5)
    tune!(b)
    run(b)
end

main()