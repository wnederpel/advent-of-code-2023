import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

data = {}
with open('data/day25.txt') as f:
    lines = f.read().splitlines()
    for line in lines:
        start, edges = line.split(": ")
        edges = edges.split(" ")
        data[start] = edges

print(data)

g = nx.Graph()

for node in data.keys():
    g.add_node(node)
    
for node in data.keys():
    for e in data[node]:
        g.add_edge(node, e)
        
print([len(c) for c in nx.connected_components(g)])

    
# nx.draw(g, with_labels=True)
# plt.show()



 