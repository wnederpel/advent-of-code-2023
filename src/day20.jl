using BenchmarkTools
using DataStructures


function parse_data()
    data = []
    machines = Dict()
    open(joinpath(dirname(@__FILE__), "../data/day13.txt")) do f
        while !eof(f)
            line = readline(f)
            machine, dests = split(line, " -> ")
            type, name = machine[1], machine[2:end]
            machines[name] = [type, Tuple(split(dests, ", ")), false]
        end
    end

    for machine in keys(machines)
        info = machines[machine]
        if info[1] == '&'
            # info[3] = Vector{Bool}(undef, 0)
            info[3] = Dict()
            for machine2 in keys(machines)
                info2 = machines[machine2]
                if machine in info2[2]
                    # The last input from machine 2 was low
                    info[3][machine2] = false
                end
            end
        end
        machines[machine] = info
    end
    return machines
end

function enqueue_new_pulses(data, queue, machine, connections, type)
    # <machine> sends a <type> pulse to <connections>  

    for other in connections
        # if <other> is conjunction, set the type of pulse in its state
        if (other in keys(data)) && data[other][1] == '&'
            data[other][3][machine] = type
        end
        enqueue!(queue, (other, type))
    end
end

function send_low_pulse(data, machine, queue)
    type, connections, state = data[machine]
    if type == 'b'
        enqueue_new_pulses(data, queue, machine, connections, false)
    elseif type == '%'
        # on off machine
        if state 
            # When state is true (on) turn off and send low pulse
            data[machine][3] = false
            enqueue_new_pulses(data, queue, machine, connections, false)
        end
        if !state 
            # When state is false (off) turn on and send high pulse
            data[machine][3] = true
            enqueue_new_pulses(data, queue, machine, connections, true)
        end
    else
        if all(values(state))
            # remembers only high -> send low
            enqueue_new_pulses(data, queue, machine, connections, false)
        else
            # otherwise -> send high
            enqueue_new_pulses(data, queue, machine, connections, true)
        end
    end
end

function send_high_pulse(data, machine, queue)
    type, connections, state = data[machine]
    if type == 'b'
        enqueue_new_pulses(data, queue, machine, connections, true)
    elseif type == '%'
        # Nothing happens
    else
        if all(values(state))
            # remembers only high -> send low
            enqueue_new_pulses(data, queue, machine, connections, false)
        else
            # otherwise -> send high
            enqueue_new_pulses(data, queue, machine, connections, true)
        end
    end
end

function send_pulse(data, machine, pulse, queue)
    # if machine == "dd"
    #     state = data[machine][3]
    #     "state of dd = $state" |> display
    # end
    if machine == "start"
        send_low_pulse(data, "roadcaster", queue)
    elseif !pulse
        send_low_pulse(data, machine, queue)
    else
        send_high_pulse(data, machine, queue)
    end
end

function solve_problem(data)
    first_encounters = Int.(zeros(4))
    neighs = ["jq", "nx", "sp", "cc"]
    for i in 1:10000
        old_data = deepcopy(data)
        
        # We keep a queue of all pulses to be send
        queue = Queue{Tuple{String, Bool}}()
        # At the start the start will send a low pulse to the broadcaster
        enqueue!(queue, ("start", false))
        # "start -low-> broadcaster" |> display




        while !isempty(queue)
            # We save the queue & pulse & iteration when processig some machine for the first time.
            name, pulse = dequeue!(queue)

            if name == "rx" || name == "output"
                if !pulse
                    println("done at $i !")
                end
                continue
            end
            if name in neighs && !any(values(data[name][3]))
                println("machine $name is sending high to dd at button press $i")
                j = findfirst(x->x==name, neighs)
                first_encounters[j] = i
            end

            send_pulse(data, name, pulse, queue)
        end
        if prod(first_encounters) != 0
            break
        end
        
    end
    
    return lcm(first_encounters)
end

function main()
    data = parse_data()
    @time sol = solve_problem(data)
    println(sol)
end

main()
